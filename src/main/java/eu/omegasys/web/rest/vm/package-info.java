/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.omegasys.web.rest.vm;
