package eu.omegasys.web.rest;

import eu.omegasys.service.ChronoswsKafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/chronosws-kafka")
public class ChronoswsKafkaResource {

    private final Logger log = LoggerFactory.getLogger(ChronoswsKafkaResource.class);

    private ChronoswsKafkaProducer kafkaProducer;

    public ChronoswsKafkaResource(ChronoswsKafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
        log.debug("REST request to send to Kafka topic the message : {}", message);
        this.kafkaProducer.sendMessage(message);
    }
}
