/**
 * Data Access Objects used by WebSocket services.
 */
package eu.omegasys.web.websocket.dto;
