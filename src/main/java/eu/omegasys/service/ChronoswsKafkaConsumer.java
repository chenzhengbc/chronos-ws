package eu.omegasys.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ChronoswsKafkaConsumer {

    private final Logger log = LoggerFactory.getLogger(ChronoswsKafkaConsumer.class);
    private static final String TOPIC = "topic_chronosws";

    @KafkaListener(topics = "topic_chronosws", groupId = "group_id")
    public void consume(String message) throws IOException {
        log.info("Consumed message in {} : {}", TOPIC, message);
    }
}
