import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChronoswsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [ChronoswsSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [ChronoswsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChronoswsSharedModule {
  static forRoot() {
    return {
      ngModule: ChronoswsSharedModule
    };
  }
}
